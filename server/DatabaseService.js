class Database {
  constructor() {
    this.preferred = [];
  }
  getPreferred() {
    return this.preferred;
  }
  addPreferred(id) {
    // Check if it exists already in the database
    if (this.preferred.indexOf(id) === -1) {
      this.preferred.push(id);
    }
  }
  removePreferred(id) {
    // Get the index of the item we want to delete
    const index = this.preferred.findIndex(item => item === id);
    if (index === -1) {
      return false;
    }
    this.preferred.splice(index, 1);
    return true;
  }
}

module.exports = new Database();
