const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const DatabaseService = require("./DatabaseService");

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:3000");
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, DELETE");
  res.header("Access-Control-Allow-Headers", "*");
  next(); // make sure we go to the next routes and don't stop here
});
// Support JSON-encoded bodies
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    // Support URL-encoded bodies
    extended: true
  })
);
app.listen(3020, function() {
  // Callback triggered when server is successfully listening
  console.log(`Server started at ${new Date()}`);
});
app.get("/preferred", function(req, res) {
  const preferred = DatabaseService.getPreferred();
  res.end(JSON.stringify({ result: preferred }));
});
app.delete("/preferred", function(req, res) {
  const idToDelete = req.body.id;
  const status = DatabaseService.removePreferred(idToDelete) ? 200 : 404;
  res.status(status).end();
});
app.post("/preferred", function(req, res) {
  const idToAdd = req.body.id;
  DatabaseService.addPreferred(idToAdd);
  res.end();
});
