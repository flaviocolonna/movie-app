# Movie's finder

## Description
This project is composed by two folders:
- client
- server

The server folder contains a NODE js program to simulate the AJAX call to get the preferred movies list.

## Installation
Please be sure to have installed NPM in your machine and then run the following command in the client and server folders:
```
npm install
```
Later, run the server as following from the /server folder:
```
node index.js
```
At the end, you are ready to test the client platform by running the following command in the /client folder:
```
npm start
```
The server is with NodeJS but there is no Database installed. The data is just kept in memory until the process lives.
The server starts on port 3020 meanwhile the client on port 3000.