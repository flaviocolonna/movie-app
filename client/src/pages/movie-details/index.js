import React, { Fragment, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import getMovieById from "../../helpers/getMovieById";
import "./style.scss";
import emptyHeart from "./empty-heart.svg";
import {
  getPreferred,
  addPreferred,
  removePreferred
} from "../../helpers/accountCalls";
import heart from "./heart.svg";
import { RButton } from "../../components/atoms";

function MovieDetails() {
  const { movieId } = useParams();
  const [movie, setMovie] = useState();
  const [error, setError] = useState(null);
  const togglePreferred = () => {
    const fn = movie.isPreferred() ? removePreferred : addPreferred;
    fn(movie.getId()).then(() => {
      movie.togglePreferred();
      setMovie(null);
      setMovie(movie);
    });
  };
  useEffect(() => {
    getMovieById(movieId).then(async currMovie => {
      try {
        const preferredMovies = await getPreferred();
        const isMoviePreferred = preferredMovies.some(item => item === movieId);
        currMovie.setAsPreferred(isMoviePreferred);
        setMovie(currMovie);
      } catch (e) {
        setError(e);
      }
    });
  }, [movieId]);
  if(error) {
    return <p className="message">Ops...Something wrong happended. Please reload.</p>;
  }
  if (!movie) {
    return <p className="message">Loading...</p>;
  }
  const heartIcon = movie.isPreferred() ? heart : emptyHeart;
  return (
    <Fragment>
      <div className="movie-wrapper">
        <div className="row movie-wrapper__heading">
          <div className="two-thirds column">
            <h1>{movie.getTitle()}</h1>
            <h2 className="section-title">Details</h2>
            <p>
              <b>Year:</b> {movie.getYear()}
            </p>
            <p>
              <b>Rated:</b> {movie.getRate()}
            </p>
            <p>
              <b>Released:</b> {movie.getReleased()}
            </p>
            <p>
              <b>Runtime:</b> {movie.getRuntime()}
            </p>
            <p>
              <b>Genre:</b> {movie.getGenre()}
            </p>
            <p>
              <b>Director:</b> {movie.getDirector()}
            </p>
            <p>
              <b>Writer:</b> {movie.getWriter()}
            </p>
            <p>
              <b>Actors:</b> {movie.getActors()}
            </p>
            <p>
              <b>Languages:</b> {movie.getLanguage()}
            </p>
            <p>
              <b>Awards:</b> {movie.getAwards()}
            </p>
            <p>
              <b>Production:</b> {movie.getProduction()}
            </p>
            <p>
              <b>BoxOffice:</b> {movie.getBoxOffice()}
            </p>
            <p>
              <b>DVD:</b> {movie.getDVD()}
            </p>
            <p>
              <b>Metascore:</b> {movie.getMetascore()}
            </p>
            <p>
              <b>Website:</b> {movie.getWebsite()}
            </p>
          </div>
          <div className="row one-third column">
            <img
              src={movie.getPoster()}
              alt="Poster"
              className="movie-wrapper__heading__poster"
            />
            <div className="movie-wrapper__heading__actions">
              <RButton icon={heartIcon} onClick={togglePreferred}></RButton>
            </div>
          </div>
        </div>
        <div className="one-third column"></div>
        <div className="row movie-wrapper__main">
          <h2 className="section-title">Plot</h2>
          <p>{movie.getPlot()}</p>
        </div>
      </div>
    </Fragment>
  );
}
export default React.memo(MovieDetails);
