import React from "react";
import axiosMocked from "axios";
import { BrowserRouter as Router } from "react-router-dom";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import MovieDetails from "./";
import MovieDetailsMock from './movieDetails.mock.json';

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});
afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("Structure to match snapshot as fail", () => {
  act(() => {
    render(
      <Router>
        <MovieDetails />
      </Router>,
      container
    );
  });

  expect(container.innerHTML).toMatchSnapshot();
});
it("Structure to match snapshot success", () => {
  axiosMocked.get.mockImplementation(url => {
    let dataToReturn;
    if (url === "http://localhost:3020/preferred") {
      dataToReturn = { result: ["1232", "312321"] };
    } else if (url.indexOf("https://www.omdbapi.com") > -1) {
      dataToReturn = MovieDetailsMock;
    }
    return Promise.resolve({
      data: dataToReturn
    });
  });
  act(() => {
    render(
      <Router>
        <MovieDetails />
      </Router>,
      container
    );
  });

  expect(container.innerHTML).toMatchSnapshot();
});
