import React from "react";
import axiosMocked from "axios";
import { BrowserRouter as Router } from "react-router-dom";
import { render, unmountComponentAtNode } from "react-dom";
import { fireEvent } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import Homepage from "./";
import MoviesListMock from "./moviesByTitle.mock.json";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});
afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("Structure to match snapshot", () => {
  act(() => {
    render(
      <Router>
        <Homepage />
      </Router>,
      container
    );
  });

  expect(container.innerHTML).toMatchSnapshot();
});
it("Page with one character displays the correct message", () => {
  act(() => {
    render(
      <Router>
        <Homepage />
      </Router>,
      container
    );
  });
  const searchInput = container.querySelector("input");
  expect(searchInput).not.toBe(null);
  fireEvent.change(searchInput, {
    target: {
      value: "B"
    }
  });
  expect(searchInput.value).toBe("B");
  expect(container.innerHTML).toMatchSnapshot();
});
it("Page with two characters displays the correct message", () => {
  act(() => {
    render(
      <Router>
        <Homepage />
      </Router>,
      container
    );
  });
  const searchInput = container.querySelector("input");
  expect(searchInput).not.toBe(null);
  fireEvent.change(searchInput, {
    target: {
      value: "Bu"
    }
  });
  expect(searchInput.value).toBe("Bu");
  expect(container.innerHTML).toMatchSnapshot();
});
it("Page with two characters displays the correct message with results", () => {
  axiosMocked.get.mockImplementation(() =>
    Promise.resolve({
      data: MoviesListMock
    })
  );
  let searchInput;
  act(() => {
    render(
      <Router>
        <Homepage />
      </Router>,
      container
    );
    searchInput = container.querySelector("input");
    fireEvent.change(searchInput, {
      target: {
        value: "Bu"
      }
    });
  });
  expect(searchInput).not.toBe(null);
  expect(searchInput.value).toBe("Bu");
  expect(container.innerHTML).toMatchSnapshot();
});
