import React, { useReducer } from "react";
import { CMainSearchInput } from "../../components/molecules";
import { searchMovieByTitle } from "../../helpers/searchMovieByTitle";
import { MoviesList } from "../../components/organisms";
import { CButton } from "../../components/atoms";
import { Errors } from "../../helpers/enums";

const initialState = {
  movies: [],
  fullLoading: false,
  userInputValue: "",
  currentPage: 1,
  error: null,
  maxPage: 1,
  isLoadingNextPage: false
};

const Events = {
  SET_MOVIES: "SET_MOVIES",
  SET_FULL_LOADING: "SET_FULL_LOADING",
  SET_USER_INPUT: "SET_USER_INPUT",
  SET_CURRENT_PAGE: "SET_CURRENT_PAGE",
  SET_MAX_PAGE: "SET_MAX_PAGE",
  SET_LOADING_NEXT_PAGE: "SET_LOADING_NEXT_PAGE",
  RESET: "RESET",
  SET_ERROR: "SET_ERROR",
  SEARCH_COMPLETED: "SEARCH_COMPLETED",
  LOADING_NEW_MOVIES: "LOADING_NEW_MOVIES"
};

function reducer(state, action) {
  switch (action.type) {
    case Events.SET_MOVIES:
      return { ...state, movies: action.payload };
    case Events.SET_FULL_LOADING:
      return { ...state, fullLoading: action.payload };
    case Events.SET_USER_INPUT:
      return { ...state, userInputValue: action.payload };
    case Events.SET_CURRENT_PAGE:
      return { ...state, currentPage: action.payload };
    case Events.SET_MAX_PAGE:
      return { ...state, maxPage: action.payload };
    case Events.SET_LOADING_NEXT_PAGE:
      return { ...state, isLoadingNextPage: action.payload };
    case Events.SET_ERROR:
      return { ...state, error: action.payload };
    case Events.SEARCH_COMPLETED:
      const { movies, totalMovies } = action.payload;
      return {
        ...state,
        fullLoading: false,
        error: null,
        movies,
        currentPage: 1,
        maxPage: Math.round(totalMovies / 10)
      };
    case Events.LOADING_NEW_MOVIES:
      return {
        ...state,
        fullLoading: true,
        error: null,
        movies: [],
        currentPage: 1,
        maxPage: 1
      };
    case Events.RESET: {
      return initialState;
    }
    default:
      return state;
  }
}
function Homepage() {
  const [state, dispatch] = useReducer(reducer, initialState);
  let loadingMovies = null;
  const onChange = event => {
    const { value } = event.target;
    dispatch({
      type: Events.SET_ERROR,
      payload: null
    });
    dispatch({
      type: Events.SET_USER_INPUT,
      payload: value
    });
    // If the user types less than 2 characters, we don't make any call
    if (value.length < 2) {
      return;
    }
    dispatch({
      type: Events.LOADING_NEW_MOVIES
    });
    searchMovieByTitle(value)
      .then(({ movies, totalMovies, error }) => {
        if (!error) {
          dispatch({
            type: Events.SEARCH_COMPLETED,
            payload: { movies, totalMovies }
          });
        }
        if (error) {
          dispatch({
            type: Events.SET_ERROR,
            payload: error
          });
        }
      })
      .catch(e => {
        dispatch({
          type: Events.SET_ERROR,
          payload: Errors.GENERAL
        });
      });
  };
  const loadMore = (event) => {
    if(state.isLoadingNextPage) {
      return event.preventDefault();
    }
    if (state.currentPage + 1 < state.maxPage) {
      dispatch({
        type: Events.SET_LOADING_NEXT_PAGE,
        payload: true
      });
      clearTimeout(loadingMovies);
      loadingMovies = setTimeout(async () => {
        try {
          const { movies: newMovies } = await searchMovieByTitle(
            state.userInputValue,
            state.currentPage + 1
          );
          dispatch({
            type: Events.SET_CURRENT_PAGE,
            payload: state.currentPage + 1
          });
          dispatch({
            type: Events.SET_MOVIES,
            payload: state.movies.concat(newMovies)
          });
          dispatch({
            type: Events.SET_LOADING_NEXT_PAGE,
            payload: true
          });
        } catch (e) {
          dispatch({
            type: Events.SET_ERROR,
            payload: Errors.GENERAL
          });
        }
      }, 200);
    }
  };
  const componentToRender = () => {
    if (state.error) {
      let message = "Ops...Something wrong happended. Please reload.";
      if (state.error === Errors.TOO_MANY_RESULTS) {
        message = "Too many results. Try to type one letter more.";
      } else if (state.error === Errors.NO_RESULTS) {
        message = "No movies found";
      }
      return <p className="message">{message}</p>;
    }
    if (state.userInputValue.length < 2) {
      return <p className="message">Type at least 2 characters</p>;
    }
    if (state.fullLoading) {
      return <p className="message">Loading...</p>;
    }
    return <MoviesList movies={state.movies}></MoviesList>;
  };
  return (
    <main>
      <CMainSearchInput onChange={onChange}></CMainSearchInput>
      {componentToRender()}
      {!state.error && state.currentPage + 1 < state.maxPage && (
        <div className="list-footer">
          <CButton onClick={loadMore}>
            {state.isLoadingNextPage ? "Loading..." : "Load more"}
          </CButton>
        </div>
      )}
    </main>
  );
}

export default Homepage;
