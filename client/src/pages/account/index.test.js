import React from "react";
import axiosMocked from "axios";
import { BrowserRouter as Router } from "react-router-dom";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import Account from "./";
import MovieDetails from './movieDetails.mock.json';

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});
afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("Structure to match snapshot", async () => {
  axiosMocked.get.mockImplementation(url => {
    let dataToReturn;
    if (url === "http://localhost:3020/preferred") {
      dataToReturn = { result: ["1232", "312321"] };
    } else if (url.indexOf("https://www.omdbapi.com") > -1) {
      dataToReturn = MovieDetails;
    }
    return Promise.resolve({
      data: dataToReturn
    });
  });
  await act(async () => {
    render(
      <Router>
        <Account />
      </Router>,
      container
    );
  });

  expect(container.innerHTML).toMatchSnapshot();
});
it("Structure to match snapshot fail case General error when getMovieById fails", async () => {
  axiosMocked.get.mockImplementation(url => {
    let dataToReturn;
    if (url === "http://localhost:3020/preferred") {
      dataToReturn = { result: ["1232", "312321"] };
    } else if (url.indexOf("https://www.omdbapi.com") > -1) {
      return Promise.reject({});
    }
    return Promise.resolve({
      data: dataToReturn
    });
  });
  await act(async () => {
    render(
      <Router>
        <Account />
      </Router>,
      container
    );
  });

  expect(container.innerHTML).toMatchSnapshot();
});
it("Structure to match snapshot fail case General error", async () => {
  axiosMocked.get.mockImplementation(() => {
    return Promise.resolve({});
  });
  await act(async () => {
    render(
      <Router>
        <Account />
      </Router>,
      container
    );
  });

  expect(container.innerHTML).toMatchSnapshot();
});
it("Structure to match snapshot success case", async () => {
  axiosMocked.get.mockImplementation(url => {
    let dataToReturn;
    if (url === "http://localhost:3020/preferred") {
      dataToReturn = { result: "1232" };
    } else if (url.indexOf("https://www.omdbapi.com") > -1) {
      dataToReturn = MovieDetails;
    }
    return Promise.resolve({
      data: dataToReturn
    });
  });
  await act(async () => {
    render(
      <Router>
        <Account />
      </Router>,
      container
    );
  });

  expect(container.innerHTML).toMatchSnapshot();
});