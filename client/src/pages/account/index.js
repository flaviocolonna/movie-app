import React, { useState, useEffect } from "react";
import { MoviesList } from "../../components/organisms";
import getMovieById from "../../helpers/getMovieById";
import { getPreferred } from "../../helpers/accountCalls";
import { Errors } from '../../helpers/enums';
import "./styles.scss";

function Account() {
  const [preferredMovies, setPreferredMovies] = useState();
  const [error, setError] = useState(null);
  useEffect(() => {
    getPreferred()
      .then(data => {
        if (Array.isArray(data)) {
          const promises = data.map(id => getMovieById(id));
          Promise.all(promises)
            .then(result => setPreferredMovies(result))
            .catch(e => setError(e));
        } else {
          setError(Errors.GENERAL);
        }
      })
      .catch(e => setError(e));
  }, []);
  const componentToRender = () => {
    if(error) {
      return <p className="message">Ops...Something wrong happended. Please reload.</p>;
    }
    if (!preferredMovies || preferredMovies.length === 0) {
      return <p className="message">You don't like any movie yet.</p>;
    }
    return <MoviesList movies={preferredMovies}></MoviesList>;
  };
  return (
    <div className="account">
      <div className="account__heading">
        <h1 className="account__heading__title">Favourites</h1>
      </div>
      {componentToRender()}
    </div>
  );
}

export default Account;
