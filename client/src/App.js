import React, { Suspense } from "react";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import { CHeader } from "./components/organisms";
import Homepage from "./pages/homepage";

const Account = React.lazy(() => import("./pages/account"));
const MovieDetails = React.lazy(() => import("./pages/movie-details"));
function Navigator() {
  return (
    <Router>
      <CHeader></CHeader>
      <Switch>
        <Route exact path="/">
          <Homepage />
        </Route>
        <Route exact path="/movie/:movieId">
          <Suspense fallback={<p className="message">Loading...</p>}>
            <MovieDetails />
          </Suspense>
        </Route>
        <Route exact path="/my-account">
          <Suspense fallback={<p className="message">Loading...</p>}>
            <Account />
          </Suspense>
        </Route>
        <Redirect from="*" to="/"></Redirect>
      </Switch>
    </Router>
  );
}

export default Navigator;
