import React from "react";
import { CNavigationLinks } from "../../molecules";
import "./style.scss";

function CHeader() {
  const NAV_LINKS = [{ text: "Home", to: "/" }, { text: "My Account", to: "/my-account" }];
  return (
    <header className="header">
      <div className="header__brand-title">
        <h1>OMDb</h1>
      </div>
      <CNavigationLinks links={NAV_LINKS} />
    </header>
  );
}

export { CHeader };
