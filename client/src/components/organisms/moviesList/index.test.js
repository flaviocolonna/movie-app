import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import MoviesListMock from "./moviesByTitle.mock.json";
import Movie from "../../../models/Movie.js";
import { MoviesList } from "./";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});
afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("Structure to match snapshot", () => {
  const movies = MoviesListMock.Search.map(movie => new Movie(movie));
  act(() => {
    render(
      <Router>
        <MoviesList movies={movies} />
      </Router>,
      container
    );
  });

  expect(container.innerHTML).toMatchSnapshot();
});
