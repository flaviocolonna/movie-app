import React, { Fragment } from "react";
import { MovieCard } from "../../molecules";
import "./styles.scss";
function MoviesList(props) {
  const { movies } = props;
  const moviesComponentToRender = movies.map((movie, index) => (
    <li className="movies-list__movie-container" key={index}>
      <MovieCard movie={movie} />
    </li>
  ));
  return (
    <Fragment>
      <ul className="movies-list">{moviesComponentToRender}</ul>
    </Fragment>
  );
}

export { MoviesList };
