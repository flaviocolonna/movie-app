import React from "react";
import "./style.scss";

function CInput(props) {
  const { className, ...rest } = props;
  return <input className={'c-input ' + className} {...rest} />;
}

export { CInput };
