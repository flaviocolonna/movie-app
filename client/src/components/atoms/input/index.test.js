import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { fireEvent } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import { CInput } from "./";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});
afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("Renders CInput renders props correctly", () => {
  const props = {
    placeholder: "Search",
    className: "input",
    id: "search-input"
  };
  const defaultClass = "c-input";
  act(() => {
    render(<CInput {...props} />, container);
  });
  const renderedTag = container.querySelector("input");
  expect(renderedTag).not.toBe(null);
  expect(renderedTag.value).toBe("");
  fireEvent.change(renderedTag, {
    target: {
      value: "x"
    }
  });
  expect(renderedTag.value).toBe("x");
  const classNames = renderedTag.getAttribute("class");
  expect(classNames).toContain(props.className);
  expect(classNames).toContain(defaultClass);
  const href = renderedTag.getAttribute("placeholder");
  expect(href).toBe(props.placeholder);
});

it("CInput to match snapshot", () => {
  const props = {
    placeholder: "Search",
    className: "input",
    id: "search-input"
  };
  act(() => {
    render(<CInput {...props} />, container);
  });

  expect(container.innerHTML).toMatchSnapshot();
});
