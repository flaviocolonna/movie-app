import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { fireEvent } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import { CButton, RButton } from "./";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});
afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("Renders CButton renders props correctly", () => {
  const customFn = jest.fn();
  const props = {
    className: "button",
    text: "click me",
    onClick: customFn,
  };
  const defaultClass = "c-button";
  act(() => {
    render(<CButton {...props}>{props.text}</CButton>, container);
  });
  const renderedTag = container.querySelector("button");
  expect(renderedTag).not.toBe(null);
  expect(renderedTag.textContent).toBe(props.text);
  fireEvent.click(renderedTag);
  expect(customFn).toHaveBeenCalled();
  const classNames = renderedTag.getAttribute("class");
  expect(classNames).toContain(props.className);
  expect(classNames).toContain(defaultClass);
});

it("Renders CRoundedButton renders props correctly", () => {
  const customFn = jest.fn();
  const props = {
    className: "button",
    text: "click me",
    icon: "test.svg",
    onClick: customFn,
  };
  const defaultClass = "c-button-rounded";
  act(() => {
    render(<RButton {...props}/>, container);
  });
  const renderedTag = container.querySelector("button");
  expect(renderedTag).not.toBe(null);
  fireEvent.click(renderedTag);
  expect(customFn).toHaveBeenCalled();
  const classNames = renderedTag.getAttribute("class");
  expect(classNames).toContain(props.className);
  expect(classNames).toContain(defaultClass);
  const imgTag = renderedTag.querySelector("img");
  expect(imgTag).not.toBe(null);
  expect(imgTag.getAttribute("src")).toBe(props.icon);
  expect(imgTag.getAttribute("class")).toBe('c-button-rounded__icon');
  expect(imgTag.getAttribute("alt")).toBe('Icon');
});
it("RButton to match snapshot", () => {
  const customFn = jest.fn();
  const props = {
    className: "button",
    text: "click me",
    icon: "test.svg",
    onClick: customFn,
  };
  act(() => {
    render(<RButton {...props} />, container);
  });

  expect(container.innerHTML).toMatchSnapshot();
});
it("CButton to match snapshot", () => {
  const props = {
    className: "button",
    text: "click me"
  };
  act(() => {
    render(<CButton {...props} />, container);
  });

  expect(container.innerHTML).toMatchSnapshot();
});
