import React from "react";
import "./style.scss";
function CButton(props) {
  const { children, className, ...rest } = props;
  return (
    <button className="c-button" {...rest}>
      {children}
    </button>
  );
}
/**
 * Rounded button with icon
 * @param {any} props
 */
function RButton(props) {
  const { children, className, icon, ...rest } = props;
  return (
    <button className={"c-button-rounded"} {...rest}>
      <img className="c-button-rounded__icon" src={icon} alt="Icon" />
    </button>
  );
}

export { CButton, RButton };
