import React from "react";
import { Link, useRouteMatch } from "react-router-dom";
import "./style.scss";

function CRouteLink(props) {
  const { to, text, isActive, hasMatchUrl } = props;
  const classes = ["link"];
  const matchUrl = useRouteMatch({
    path: to,
    exact: true
  });
  if (isActive || (hasMatchUrl && matchUrl)) {
    classes.push("link--active");
  }
  return (
    <Link to={to} className={classes.join(" ")}>
      {text}
    </Link>
  );
}

export { CRouteLink };
