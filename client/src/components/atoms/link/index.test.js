import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { CRouteLink } from "./";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});
afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("Renders CLink renders text correctly", () => {
  const link = {
    to: "/test",
    text: "Test",
  };
  act(() => {
    render(
      <Router>
        <CRouteLink {...link} />
      </Router>,
      container
    );
  });
  const renderedTag = container.querySelector("a");
  expect(renderedTag).not.toBe(null);
  const classNames = renderedTag.getAttribute("class");
  expect(classNames).toBe('link');
  const href = renderedTag.getAttribute("href");
  expect(href).toBe(link.to);
  const renderedText = renderedTag.textContent;
  expect(renderedText).toBe(link.text);
});


it("Renders CLink active changes class", () => {
  const link = {
    to: "/match-url",
    text: "Test",
    isActive: true,
  };
  act(() => {
    render(
      <Router>
        <CRouteLink {...link} />
      </Router>,
      container
    );
  });
  const renderedTag = container.querySelector("a");
  expect(renderedTag).not.toBe(null);
  const classNames = renderedTag.getAttribute("class");
  expect(classNames).toBe('link link--active');
  const href = renderedTag.getAttribute("href");
  expect(href).toBe(link.to);
  const renderedText = renderedTag.textContent;
  expect(renderedText).toBe(link.text);
});