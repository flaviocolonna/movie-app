import React from "react";
import { CRouteLink } from "../../atoms/";
import "./style.scss";

function CNavigationLinks(props) {
  const { links } = props;
  const linksToRender = links.map(({ to, text }, index) => {
    return <CRouteLink key={index} to={to} text={text} hasMatchUrl />;
  });
  return <nav className="nav-container">{linksToRender}</nav>;
}

export { CNavigationLinks };
