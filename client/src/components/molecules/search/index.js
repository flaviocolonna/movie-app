import React from "react";
import { CInput } from "../../atoms";
import "./style.scss";

function CMainSearchInput(props) {
  const { onChange } = props;
  return (
    <div className="row search-container">
      <CInput
        onChange={onChange}
        placeholder={"Search"}
        type={"text"}
        className="one-half column search-container__input"
      />
    </div>
  );
}

export { CMainSearchInput };
