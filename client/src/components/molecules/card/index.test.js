import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { MovieCard } from "./";
import MockMovie from "./movieDetails.mock.json";
import Movie from "../../../models/Movie";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});
afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("Snapshot match correctly", () => {
  act(() => {
    render(
      <Router>
        <MovieCard movie={new Movie(MockMovie)} />
      </Router>,
      container
    );
  });

  expect(container.innerHTML).toMatchSnapshot();
});
