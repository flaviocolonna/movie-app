import React from "react";
import "./style.scss";
import { Link } from "react-router-dom";
function MovieCardBasic(props) {
  const { movie } = props;
  return (
    <div className="card">
      <img src={movie.getPoster()} alt={`${movie.getTitle()} movie poster`} className="card__poster" />
      <span className="card__back-opacity"></span>
      <div className="card__heading">
        <h2 className={"movie__heading__title"} title={movie.getTitle()}>{movie.getTitle()}</h2>
      </div>
      <div className="card__details">
        <Link className="button c-button" to={`/movie/${movie.getId()}`}>Details</Link>
      </div>
    </div>
  );
}

const MovieCard = React.memo(MovieCardBasic);

export { MovieCard };
