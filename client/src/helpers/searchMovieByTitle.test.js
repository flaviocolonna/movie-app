import axiosMocked from "axios";
import { searchMovieByTitle } from "./searchMovieByTitle";
import MockMovies from "./__mocks__/moviesByTitle.mock.json";

it("Get Movie By Search value returns list of movies", async () => {
  axiosMocked.get.mockImplementationOnce(() =>
    Promise.resolve({
      data: MockMovies
    })
  );
  const { movies, totalMovies, currentPage } = await searchMovieByTitle("1234");
  expect(MockMovies.Search.length).toBe(movies.length);
  expect(MockMovies.totalResults).toBe(totalMovies);
  expect(currentPage).toBe(1);
});
it("Get Movie By Search value handle no results", async () => {
  axiosMocked.get.mockImplementationOnce(() =>
    Promise.resolve({
      data: { Response: "False", Error: "Movie not found." }
    })
  );
  const { error } = await searchMovieByTitle("1234");
  expect(error).toBe("NO_RESULTS");
});
it("Get Movie By Search value handle too many results", async () => {
  axiosMocked.get.mockImplementationOnce(() =>
    Promise.resolve({
      data: { Response: "False", Error: "Too many results." }
    })
  );
  const { error } = await searchMovieByTitle("1234");
  expect(error).toBe("TOO_MANY_RESULTS");
});
