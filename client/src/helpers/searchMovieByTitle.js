import { IMDB_BASE_URL, IMDB_API_KEY } from "../configs";
import axios from "axios";
import Movie from "../models/Movie";
import { Errors } from './enums';

const searchMovieByTitle = (title, page = 1) => {
  const url = `${IMDB_BASE_URL}/?s=${title}&apikey=${IMDB_API_KEY}&type=movie&page=${page}`;
  return axios.get(url).then(({ data }) => {
    if (!data || (data.Response === 'False' && data.Error.indexOf('Movie not found') > -1)) {
      return {
        error: Errors.NO_RESULTS,
      };
    }
    if(data.Response === 'False' && data.Error.indexOf('Too many results') > -1) {
      return {
        error: Errors.TOO_MANY_RESULTS,
      };
    }
    return {
      movies: data.Search.map(movie => new Movie(movie)),
      totalMovies: data.totalResults,
      currentPage: page,
    };
  });
};

export { searchMovieByTitle };
