import axiosMocked from "axios";
import getMovieById from './getMovieById';
import MockMovie from './__mocks__/movieDetails.mock.json';

it('Get Movie By Id returns movie instance', async () => {
  axiosMocked.get.mockImplementationOnce(() =>
    Promise.resolve({
      data: MockMovie
    })
  );
  const movie = await getMovieById('1234');
  expect(movie).not.toBe(null);
  expect(movie.getTitle()).toBe(MockMovie.Title);
  expect(movie.getYear()).toBe(MockMovie.Year);
  expect(movie.getRate()).toBe(MockMovie.Rated);
  expect(movie.getReleased()).toBe(MockMovie.Released);
  expect(movie.getRuntime()).toBe(MockMovie.Runtime);
  expect(movie.getGenre()).toBe(MockMovie.Genre);
  expect(movie.getDirector()).toBe(MockMovie.Director);
  expect(movie.getWriter()).toBe(MockMovie.Writer);
  expect(movie.getActors()).toBe(MockMovie.Actors);
  expect(movie.getPlot()).toBe(MockMovie.Plot);
  expect(movie.getLanguage()).toBe(MockMovie.Language);
  expect(movie.getCountry()).toBe(MockMovie.Country);
  expect(movie.getAwards()).toBe(MockMovie.Awards);
  expect(movie.getRatings()).toBe(MockMovie.Ratings);
  expect(movie.getMetascore()).toBe(MockMovie.Metascore);
  expect(movie.getImdbRating()).toBe(MockMovie.imdbRating);
  expect(movie.getImdbVotes()).toBe(MockMovie.imdbVotes);
  expect(movie.getId()).toBe(MockMovie.imdbID);
  expect(movie.getType()).toBe(MockMovie.Type);
  expect(movie.getDVD()).toBe(MockMovie.DVD);
  expect(movie.getBoxOffice()).toBe(MockMovie.BoxOffice);
  expect(movie.getWebsite()).toBe(MockMovie.Website);
  expect(movie.getPoster()).toBe(MockMovie.Poster);
  expect(movie.getProduction()).toBe(MockMovie.Production);
});
