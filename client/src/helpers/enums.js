const Errors = {
  TOO_MANY_RESULTS: 'TOO_MANY_RESULTS',
  NO_RESULTS: 'NO_RESULTS',
  GENERAL: 'GENERAL'
};

export { Errors };