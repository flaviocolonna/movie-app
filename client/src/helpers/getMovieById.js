import axios from "axios";
import { IMDB_BASE_URL, IMDB_API_KEY } from "../configs";
import Movie from "../models/Movie";

const getMovieById = id => {
  const url = `${IMDB_BASE_URL}/?i=${id}&apikey=${IMDB_API_KEY}`;
  return axios.get(url).then(({ data }) => new Movie(data));
};

export default getMovieById;
