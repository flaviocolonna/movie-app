import { getPreferred, addPreferred, removePreferred } from './accountCalls';
import axiosMocked from "axios";

it("Get Preferred Movies", async () => {
  axiosMocked.get.mockImplementationOnce(() =>
    Promise.resolve({
      data: { result: ['1232','312321'] }
    })
  );
  const preferredMovies = await getPreferred();
  expect(preferredMovies).toEqual(['1232','312321']);
});
it("Set a movie as preferred", async () => {
  axiosMocked.post.mockImplementationOnce(() =>
    Promise.resolve({
      status: 200
    })
  );
  const result = await addPreferred("1234");
  expect(result.status).toBe(200);
});
it("Remove a movie from preferred", async () => {
  axiosMocked.delete.mockImplementationOnce(() =>
    Promise.resolve({
      status: 200
    })
  );
  const result = await removePreferred("1234");
  expect(result.status).toBe(200);
});
