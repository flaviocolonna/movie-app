import axios from "axios";
import { C_BASE_URL } from "../configs";

const getPreferred = () =>
  axios.get(C_BASE_URL + "/preferred").then(({ data: { result } }) => result);
const addPreferred = id => axios.post(C_BASE_URL + "/preferred", { id });
const removePreferred = id =>
  axios.delete(C_BASE_URL + "/preferred", {
    headers: {
      "Content-Type": "application/json"
    },
    data: {
      id
    }
  });

export { getPreferred, addPreferred, removePreferred };
