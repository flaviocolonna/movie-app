import MockMovie from "./movieDetails.mock.json";
import Movie from "./Movie";

let movie;
beforeEach(() => {
  movie = new Movie(MockMovie);
})
it("Movie model is created correctly", () => {
  expect(movie).not.toBe(null);
});
it("Movie model contains the correct data", () => {
  expect(movie.getTitle()).toBe(MockMovie.Title);
  expect(movie.getYear()).toBe(MockMovie.Year);
  expect(movie.getRate()).toBe(MockMovie.Rated);
  expect(movie.getReleased()).toBe(MockMovie.Released);
  expect(movie.getRuntime()).toBe(MockMovie.Runtime);
  expect(movie.getGenre()).toBe(MockMovie.Genre);
  expect(movie.getDirector()).toBe(MockMovie.Director);
  expect(movie.getWriter()).toBe(MockMovie.Writer);
  expect(movie.getActors()).toBe(MockMovie.Actors);
  expect(movie.getPlot()).toBe(MockMovie.Plot);
  expect(movie.getLanguage()).toBe(MockMovie.Language);
  expect(movie.getCountry()).toBe(MockMovie.Country);
  expect(movie.getAwards()).toBe(MockMovie.Awards);
  expect(movie.getRatings()).toBe(MockMovie.Ratings);
  expect(movie.getMetascore()).toBe(MockMovie.Metascore);
  expect(movie.getImdbRating()).toBe(MockMovie.imdbRating);
  expect(movie.getImdbVotes()).toBe(MockMovie.imdbVotes);
  expect(movie.getId()).toBe(MockMovie.imdbID);
  expect(movie.getType()).toBe(MockMovie.Type);
  expect(movie.getDVD()).toBe(MockMovie.DVD);
  expect(movie.getBoxOffice()).toBe(MockMovie.BoxOffice);
  expect(movie.getWebsite()).toBe(MockMovie.Website);
  expect(movie.getPoster()).toBe(MockMovie.Poster);
  expect(movie.getProduction()).toBe(MockMovie.Production);
});
it('Movie toggle preferences', () => {
  expect(typeof movie.isPreferred).toBe('function');
  expect(movie.isPreferred()).toBeFalsy();
  expect(typeof movie.togglePreferred).toBe('function');
  movie.togglePreferred();
  expect(movie.isPreferred()).toBeTruthy();
  expect(typeof movie.setAsPreferred).toBe('function');
  movie.setAsPreferred(false);
  expect(movie.isPreferred()).toBeFalsy();
  expect(movie.preferred).toBeFalsy();
});
it('Movie has default poster', () => {
  const movieDefaultPoster = new Movie({
    ...MockMovie,
    Poster: 'N/A',
  })
  expect(movieDefaultPoster.getPoster()).not.toBe('N/A');
});
