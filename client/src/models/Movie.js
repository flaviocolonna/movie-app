import videoIcon from "../assets/icons/video.png";

function Movie(props) {
  const {
    Title,
    Year,
    Rated,
    Released,
    Runtime,
    Genre,
    Director,
    Writer,
    Actors,
    Plot,
    Language,
    Country,
    Awards,
    Ratings,
    Metascore,
    imdbID,
    imdbRating,
    imdbVotes,
    Type,
    DVD,
    BoxOffice,
    Production,
    Website,
    Poster,
  } = props;
  this.preferred = false;
  return {
    getTitle() {
      return Title;
    },
    getYear() {
      return Year;
    },
    getRate() {
      return Rated;
    },
    getReleased() {
      return Released;
    },
    getRuntime() {
      return Runtime;
    },
    getGenre() {
      return Genre;
    },
    getDirector() {
      return Director;
    },
    getWriter() {
      return Writer;
    },
    getActors() {
      return Actors;
    },
    getPlot() {
      return Plot;
    },
    getLanguage() {
      return Language;
    },
    getCountry() {
      return Country;
    },
    getAwards() {
      return Awards;
    },
    getRatings() {
      return Ratings;
    },
    getMetascore() {
      return Metascore;
    },
    getImdbRating() {
      return imdbRating;
    },
    getImdbVotes() {
      return imdbVotes;
    },
    getId() {
      return imdbID;
    },
    getType() {
      return Type;
    },
    getDVD() {
      return DVD;
    },
    getBoxOffice() {
      return BoxOffice;
    },
    getProduction() {
      return Production;
    },
    getWebsite() {
      return Website;
    },
    getPoster() {
      return Poster.indexOf('N/A') === -1 ? Poster : videoIcon;
    },
    togglePreferred() {
      this.preferred = !this.preferred;
    },
    setAsPreferred(val) {
      this.preferred = val;
    },
    isPreferred() {
      return this.preferred;
    }
  };
}
export default Movie;
